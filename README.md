Simple CSS Shrinker is an extremely simple CSS file compressor written in Go.
It's useful for reducing CSS file size for the purpose of conserving web
application bandwidth.

Basics
======
Simple CSS Shrinker currently does not support any @ directives and does not
try to fail gracefully for invalid CSS.  In both cases, it will perform
unexpectedly.

Optimizations
=============
Currently, Simple CSS Shrinker only removes comments, unnecessary whitespace,
and rules with empty bodies (such as "a {}").

Running
=======
Running Simple CSS Shrinker requires that you have Go installed.

	> GOPATH="`pwd`" go run src/shrink/shrink.go < input.css > compressed.css

Building
========
Building also requires that Go be installed.

	> GOPATH="`pwd`" go install shrink

The resulting binary will be at bin/shrink.
