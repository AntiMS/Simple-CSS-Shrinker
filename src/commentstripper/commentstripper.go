/*
	This package defines a CommentStripper struct which must be initialized with an io.Reader and which itself implements
	io.Reader.  When read from, the CommentStripper produces the contents of the underlying io.Reader with all C-style
	block comment blocks (delimited with \/\* and \*\/) removed.

	A simple example usage:

		cs := commentstripper.New(SomeReader)
		bytes := ioutil.ReadAll(cs)
*/
package commentstripper

import (
	"bytes"
	"io"
)

// Values which indicate the current state of the CommentStripper.
const (
	DEFAULT    = iota // Not in a comment.
	IN_COMMENT        // In a comment which hasn't yet been terminated.
	LAST_SLASH        // Not in a comment, but the last read ended on a forward slash.  Suspected comment start.
	LAST_SPLAT        // In a comment, and the last character read was a splat.  Suspected comment end.
)

// A CommentStripper is a struct which implements io.Reader which reads from an
// io.Reader and whose output is identical to its input save for the removal of
// all C-style (/* */) block comments.
type CommentStripper struct {
	src   *io.Reader // The io.Reader from which to read the un-stripped input.
	state int        // The current state of the CommentStripper.  Indicates whether the next portion read should be treated as a comment or not.
}

// Places content read from the io.Reader into the given byte slice, removing
// all C-style block comments as it goes.  There is one known bug.  If the
// input from src ends with a forward slash character and that forward slash
// character is not part of a closing comment block indicator (*/) properly
// matched with an open comment block indicator (/*), that forward slash
// character will not be produced.
func (c *CommentStripper) Read(buf []byte) (int, error) {
	var (
		n int
		e error
	)

	// Read from the io.Reader.
	if c.state == LAST_SLASH {
		// The last byte read was a slash character which is suspected to be
		// the first portion of an opening comment block indicator (/*).
		n, e = (*c.src).Read(buf[:len(buf)-1])
		if buf[0] == '*' {
			// It is indeed an opening comment block indicator.
			c.state = IN_COMMENT
		} else {
			// It's not an opening comment block indicator.  We'll put in the
			// forward slash we didn't output last time.
			copy(buf[1:], buf)
			buf[0] = '/'
			n += 1
		}
	} else {
		// Just read from the io.Reader normally.
		n, e = (*c.src).Read(buf)
	}

	if n == 0 {
		return n, e
	}

	// The index at which a comment starts in this chunk.  Set later.
	var commentStart int

	// last byte read from the io.Reader.
	lastByteRead := buf[n-1]

	switch c.state {
	case IN_COMMENT:
		// The comment portion starts from the beginning of this portion.
		commentStart = 0
	case LAST_SPLAT:
		if buf[0] == '/' {
			// The comment just ended at the beginning of this portion.
			copy(buf[0:n-1], buf[1:n])
			n--
			commentStart = bytes.Index(buf, []byte{'/', '*'})
		} else {
			commentStart = 0
		}
	case LAST_SLASH:
		if buf[0] == '*' {
			// The comment just started at the beginning of this portion.
			commentStart = 0
		} else {
			commentStart = -1
		}
	default: // case DEFAULT:
		// We'll just look for an opening comment block indicator (/*).
		commentStart = bytes.Index(buf, []byte{'/', '*'})
	}

	// Reset the state.
	c.state = DEFAULT

	for commentStart != -1 && c.state == DEFAULT {
		// Find the end of the comment (if it exists in this portion).
		if commentEnd := bytes.Index(buf[commentStart:n], []byte{'*', '/'}); commentEnd == -1 {
			if lastByteRead == '*' && commentStart != n-2 {
				// We're still in a comment, but our last byte read was a
				// splat.  Might be the the start of a closing comment block
				// indicator (*/).
				c.state = LAST_SPLAT
			} else {
				c.state = IN_COMMENT
			}
			n = commentStart
		} else {
			// We found a closing comment block indicator (*/).
			commentEnd += commentStart + 2

			// Move everything following the comment back to where it should be.
			copy(buf[commentStart:], buf[commentEnd:])
			if commentEnd == n {
				// We don't want to mistake the forward slash on our closing
				// comment block indicator (*/) as the opening of another
				// comment.
				lastByteRead = ' '
			}
			// Adjust the output length.
			n -= commentEnd - commentStart
		}

		// Look for another opening comment block indicator (*/).
		commentStart = bytes.Index(buf[:n], []byte{'/', '*'})
	}

	if c.state == DEFAULT && lastByteRead == '/' {
		// We don't want to output that slash at the end in case it's the
		// opening of a new comment block.
		n--
		c.state = LAST_SLASH
	}

	return n, e
}

// Creates a new CommentStripper based on the given io.Reader.
func New(src io.Reader) *CommentStripper {
	return &CommentStripper{src: &src, state: DEFAULT}
}
