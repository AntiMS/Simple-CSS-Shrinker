package commentstripper

import (
	"bytes"
	"io/ioutil"
	"testing"
)

// A struct for holding the input and corresponding expected output.
type stripCase struct {
	Input  []byte // The input.
	Output []byte // The expected output.
}

// All cases to test.  The test function below will iterate over this, testing
// each one in turn.
var cases []stripCase = []stripCase{
	// A simple, commentless string.
	stripCase{
		[]byte("basic test"),
		[]byte("basic test"),
	},
	// A string with a single comment.
	stripCase{
		[]byte("basic /* woot */test"),
		[]byte("basic test"),
	},
	// Two comments.
	stripCase{
		[]byte("/* foo */basic /* woot */test"),
		[]byte("basic test"),
	},
	// A tricky case.  Makes sure we can't easily confuse closing (*/) and
	// opening (/*) indicators.
	stripCase{
		[]byte("/* foo */* abc */basic /* woot */test"),
		[]byte("* abc */basic test"),
	},
	// Lots of forward slashes to trip it up.
	stripCase{
		[]byte("///////* asdf */////// "),
		[]byte("//////////// "),
	},
	// A long comment taking up the whole string.
	stripCase{
		[]byte("/*              asdf          */"),
		[]byte(""),
	},
	// Something of a real world case.
	stripCase{
		[]byte("/*\n\tThe body\n */\nbody {\n\tcolor: #ff0000;\n}\n\n/*\n\tLinks\n */\na {\n\ttext-decoration: none;\n}\n"),
		[]byte("\nbody {\n\tcolor: #ff0000;\n}\n\n\na {\n\ttext-decoration: none;\n}\n"),
	},
	//stripCase{ // This will fail.  Known bug.  See commentstripper.go .  Of
	//course, if your CSS file ends with a forward slash, something is wrong,
	//but it would still be good to fix this.
	//	[]byte("///"),
	//	[]byte("///"),
	//},
}

// A unit test function for the CommentStripper struct in this package.
func TestStripper(t *testing.T) {
	for _, c := range cases {
		buf := bytes.NewBuffer(c.Input)

		// Create our CommentStripper.
		cs := New(buf)

		// Read from it.
		out, _ := ioutil.ReadAll(cs)

		// Compare our results.
		if !bytes.Equal(c.Output, out) {
			t.Errorf("\"%s\" -> \"%s\", not \"%s\"", c.Input, c.Output, out)
		}
	}
}
