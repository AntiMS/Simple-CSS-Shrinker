/*
	This package contains functions and related infrastructure for parsing
	CSS (minus comments and @ directives) from an io.Reader into objects
	suitable for manipulation.
*/
package parsecss

import (
	"bufio"
	"bytes"
	"io"
)

type Item interface {
	ByteSlice() []byte
	String() string
}

// A CSS '@' query directive (e.g. "@media screen and (min-width: 360px) { p { color : #ff0000; } a { color : #000000; } }").
type query struct {
	Query []byte
	Items []Item
}

func (q *query) ByteSlice() []byte {
	out := make([]byte, 0, 3*len(q.Query))
	out = append(out, '@')
	out = append(out, q.Query...)
	out = append(out, '{')
	for _, i := range q.Items {
		out = append(out, i.ByteSlice()...)
	}
	out = append(out, '}')
	return out
}

func (q *query) String() string {
	return string(q.ByteSlice())
}

// A CSS directive (e.g. "color: #ff0000;").
type pair struct {
	Key   []byte // The property name of the directive (e.g. "color").
	Value []byte // The value of the directive (e.g. "#ff0000").
}

// A CSS rule (e.g. "p { color: #ff0000; }".
type rule struct {
	Selector []byte // The selector portion of the rule (e.g. "p").
	Pairs    []pair // A slice of pair objects representing the contained directives.
}

// Returns the CSS rule in a text form as compact as possible as a byte slice.
// For instance, []byte("p{color:#ff0000;}").
func (r *rule) ByteSlice() []byte {
	l := len(r.Selector) + 2
	for _, p := range r.Pairs {
		l += len(p.Key) + len(p.Value) + 2
	}
	out := make([]byte, l)
	copy(out, r.Selector)
	out[len(r.Selector)] = '{'
	out[l-1] = '}'
	i := len(r.Selector) + 1
	for _, p := range r.Pairs {
		i += copy(out[i:], p.Key)
		out[i] = ':'
		i++
		i += copy(out[i:], p.Value)
		out[i] = ';'
		i++
	}
	return out
}

// Returns the CSS rule in a text form as compact as possible as a string.  For
// instance, "p{color:#ff0000;}".
func (r *rule) String() string {
	return string(r.ByteSlice())
}

// Given a reader, parses the CSS read from the reader into a slice of rule
// (defined above) objects.  This function doesn't support the entirety of the
// CSS spec.  It will perform unexpectedly on all comments and "@" directives.
func Parse(src io.Reader) []Item {
	// Create a buffered reader.
	in := bufio.NewReader(src)

	return parse(in)
}

func parse(in *bufio.Reader) []Item {
	// Create the slice of items to be returned.  (This slice will be extended as necessary.)
	items := make([]Item, 0)

	// Parse each of the rules one by one.
	for {
		c, e := in.ReadByte()
		if e != nil || c == '}' {
			break
		}
		if c == ' ' || c == '\t' || c == '\n' || c == '\r' {
			continue
		}
		sel, e := in.ReadBytes('{')
		sel = bytes.Join([][]byte{[]byte{c}, sel}, []byte{})
		sel = sel[:len(sel)-1]
		sel = bytes.TrimSpace(sel)
		var t Item
		if c == '@' {
			t = parseQuery(sel[1:], in)
		} else {
			t = parseRule(sel, in)
		}
		if t != nil {
			items = append(items, t)
		}
	}

	return items
}

func parseQuery(sel []byte, in *bufio.Reader) Item {
	sel = bytes.TrimSpace(sel)
	osel := make([]byte, 0, len(sel))

	pw := false
	for _, c := range sel {
		if c == ' ' || c == '\t' || c == '\n' || c == '\r' {
			if !pw {
				osel = append(osel, ' ')
			}
			pw = true
		} else {
			pw = false
			osel = append(osel, c)
		}
	}

	return &query{osel, parse(in)}
}

func parseRule(sel []byte, in *bufio.Reader) Item {
	// Clean up the selector.
	sel = bytes.TrimSpace(sel)
	sel = bytes.Replace(sel, []byte{'\t'}, []byte{' '}, -1)
	sel = bytes.Replace(sel, []byte{'\n'}, []byte{' '}, -1)
	for si := bytes.IndexByte(sel, ' '); si != -1; si = bytes.IndexByte(sel[si+2:], ' ') + si + 2 {
		lsi := bytes.IndexFunc(sel[si+1:], func(c rune) bool { return c != ' ' })
		if lsi == -1 {
			// No non-space was found.
			break
		} else if lsi == 0 {
			// The very next character was a non-space.
			continue
		}
		copy(sel[si+1:], sel[si+lsi+1:])
		sel = sel[:len(sel)-lsi]
	}
	sel = bytes.Replace(sel, []byte{',', ' '}, []byte{','}, -1)
	sel = bytes.Replace(sel, []byte{' ', ','}, []byte{','}, -1)
	sel = bytes.Replace(sel, []byte{'>', ' '}, []byte{'>'}, -1)
	sel = bytes.Replace(sel, []byte{' ', '>'}, []byte{'>'}, -1)

	// Read the body portion.
	body, _ := in.ReadBytes('}')
	// Clean up the body.
	body = bytes.TrimSpace(body[:len(body)-1])

	if len(body) == 0 {
		// This rule doesn't do anything.  It's useless.  No need to
		// include it in the output.
		return nil
	}

	// Create the slice of pairs to store in the rule.  (This slice will be
	// extended as necessary.)
	pairs := make([]pair, 0)

	// Iterate over the directives in the body.
	for _, p := range bytes.Split(body, []byte{';'}) {
		// Clean up the pair.
		p = bytes.TrimSpace(p)
		i := bytes.Index(p, []byte{':'})
		if i == -1 {
			// Hmm.  There's no colon in this pair.  Something's wrong.
			// We'll just silently omit it.
			continue
		}

		// Extend our slice of pairs with the new directive.
		pairs = append(pairs, pair{bytes.TrimSpace(p[:i]), bytes.TrimSpace(p[i+1:])})
	}

	return &rule{sel, pairs}
}
