package parsecss

import (
	"bytes"
	"testing"
)

// A struct for holding the input and corresponding expected output.
type parseCase struct {
	Input  []byte   // The input.
	Output [][]byte // A slice of byte slices of expected output.
}

// All cases to test.  The test function below will iterate over this, testing
// each one in turn.
var cases []parseCase = []parseCase{
	// Tests for handling unnecessary whitespace.
	parseCase{
		[]byte("\n\n \ta   {\n\n\n\t\n b         \n: \t c     ;  }\n\n\n"),
		[][]byte{
			[]byte("a{b:c;}"),
		},
	},
	// Tests for multiple rules in one file.
	parseCase{
		[]byte("a{b:c;}d{e:f;}g{h:i;}"),
		[][]byte{
			[]byte("a{b:c;}"),
			[]byte("d{e:f;}"),
			[]byte("g{h:i;}"),
		},
	},
	// Something of a "real world" case.  Tests with real-world selectors and directives.
	parseCase{
		[]byte("body {\n  background-color: #ff0000;\n  color: #00ff00;\n  width: 987px;\n}\n\na#link {\n  text-decoration: none;\n}"),
		[][]byte{
			[]byte("body{background-color:#ff0000;color:#00ff00;width:987px;}"),
			[]byte("a#link{text-decoration:none;}"),
		},
	},
	// Tests selector cleaning.
	parseCase{
		[]byte("a\n\n\n\nb , c \n d{e:f;}"),
		[][]byte{
			[]byte("a b,c d{e:f;}"),
		},
	},
	// Tests for empty rules.
	parseCase{
		[]byte("a{b:c;}d{}e{f:g;}"),
		[][]byte{
			[]byte("a{b:c;}"),
			[]byte("e{f:g;}"),
		},
	},
}

// A unit test function for the Parse function in this package.
func TestParser(t *testing.T) {
	// Iterate over the cases defined above.
	for i, c := range cases {
		// Run the Parse function.
		rules := Parse(bytes.NewBuffer(c.Input))

		if len(rules) != len(c.Output) {
			t.Errorf("parsecss.Parse() found %d rules, expected %d for test case %d.", len(rules), len(c.Output), i)
		}

		// Compare the rules individually with the expected output.
		for j, r := range c.Output {
			if !bytes.Equal(r, rules[j].ByteSlice()) {
				t.Errorf("Incorrect rule parsing for case %d rule %d.  Expected \"%s\", was \"%s\".", i, j, r, rules[j].ByteSlice())
			}
		}
	}
}
