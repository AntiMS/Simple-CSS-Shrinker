/*
	A command for compressing CSS files.  This command doesn't work for all CSS
	files.  It performs unexpectedly for any "@" directive.  It also performs
	unexpectedly for invalid CSS.

	This command only strips comments, eliminates unnecessary whitespace, and
	removes empty rules.

	Usage:
		shrink < file.css > compressed.css
*/
package main

import (
	"commentstripper"
	"io"
	"os"
	"parsecss"
)

// Prints usage information for this command.
func usage() {
	os.Stderr.WriteString("Usage:\n")
	os.Stderr.WriteString("\t" + os.Args[0] + " < file.css > compressed.css\n")
}

// Reads CSS from in, compresses it, and writes it to out.
func shrink(in io.Reader, out io.Writer) {
	// Parse the css.
	rs := parsecss.Parse(commentstripper.New(in))

	// Write each item, one by one.
	for _, r := range rs {
		out.Write(r.ByteSlice())
	}
}

// The main function.  Ran when this command is invoked.
func main() {
	if len(os.Args) > 1 {
		// This command takes no arguments.
		usage()
		os.Exit(1)
	}

	shrink(os.Stdin, os.Stdout)
}
